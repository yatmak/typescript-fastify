# Typescript Fastify Project

## 실행

```bash
yarn
yarn build
yarn start
```

## 참고

- [fastify-decorators](https://github.com/L2jLiga/fastify-decorators)
