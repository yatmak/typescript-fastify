import { app } from '../src/app'
import jwt from 'jsonwebtoken'

const token = jwt.sign(
  {
    sub: '7893',
    roles: [
      {
        no: 9101890,
        name: 'ROLE_BIZ_BASIC',
      },
    ],
    iat: 1621932534,
    exp: 1622537334,
  },
  process.env.JWT_SECRET || 'secret',
)

describe('Server Availability', () => {
  test('루트 응답', async (done) => {
    // Fastify HTTP injection: https://www.fastify.io/docs/latest/Testing/
    const response = await app.inject({
      method: 'GET',
      url: '/',
    })
    expect(response.statusCode).toEqual(200)
    done()
  })
})

afterAll((done) => {
  done()
})
