import fastify from 'fastify'
import { bootstrap } from 'fastify-decorators'
import { resolve } from 'path'

const app = fastify()

const host = process.env.HOST || '127.0.0.1'
const port: number = Number.parseInt(process.env.PORT) || 5000
const env = process.env.NODE_ENV || 'development'

app.register(bootstrap, {
  directory: resolve(__dirname, 'api'),
  mask: /\.handler\./,
})

export { app, env, host, port }
