import {
  Controller,
  ErrorHandler,
  GET,
  RequestHandler,
} from 'fastify-decorators'
/**
 *
 */

class TokenNotFoundError extends Error {}

@Controller('/')
export default class IndexHandler extends RequestHandler {
  @GET('/')
  async handle() {
    return 'readiness endpoint'
  }

  @ErrorHandler(TokenNotFoundError)
  handleTokenNotFound(error: TokenNotFoundError, request, reply) {
    reply.status(403).send({ message: 'You have no access' })
  }
}
