import logger from './logger'
import { app, env, host, port } from './app'

async function start() {
  app.listen(port, (err, address) => {
    if (err) {
      logger.error('Server failed with Error: ', err)
      process.exit(1)
    }
    logger.log({
      level: 'info',
      message: `[${env}] Server listening on http://${host}:${port}, API Base http://${host}:${port}/api`,
    })
  })
}

start()
